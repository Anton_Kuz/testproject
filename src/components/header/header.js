import React from 'react';
import { Link } from 'react-router-dom';

import './header.css';


const Header = () => {
  return (
    <div>
      <ul>
        <li>
          <Link to="/org">Организации</Link>
        </li>
        <li>
          <Link to="/offices">Подразделения</Link>
        </li>
        <li>
          <Link to="/workers">Работники</Link>
        </li>
      </ul>
    </div>
  );
};

export default Header;
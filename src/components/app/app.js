import React, { Component } from "react";

import Header from "../header";

import ItemListOffisec from "../item-list-offices";
import ItemListOrg from "../item-list-org";
import ItemListWorkers from "../item-list-workers";
import { BrowserRouter as Router, Route } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <div>
        <Route path="/" component={Header} />
        <Route path="/org" component={ItemListOffisec} />
        <Route path="/offices" component={ItemListOrg} />
        <Route path="/workers" component={ItemListWorkers} />
      </div>
    </Router>
  );
};
export default App;

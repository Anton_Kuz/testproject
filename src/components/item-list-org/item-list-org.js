import React from "react";

const ItemListOrg = () => {
  return (
    <div>
      <h2>Список организаций</h2>
      <table>
        <thead>
          <th>Идентификатор</th>
          <th>Название</th>
          <th>Адрес</th>
          <th>ИНН</th>
        </thead>
      </table>
    </div>
  );
};
export default ItemListOrg;

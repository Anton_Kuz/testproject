/* export default class SwapiService {
  _apiBase = "http://5cbeb34f06a6810014c65df1.mockapi.io";
  async getResource(url) {
    const res = await fetch(`${this._apiBase}${url}`);

    if (!res.ok) {
      throw new Error(`Error ${url}` + `, received ${res.status}`);
    }
    return await res.json();
  }
  async getAllOrganization() {
    const res = await this.getResource(`/org/`);
    return res;
  }
  getOrg(id) {
    return this.getResource(`/org/${id}/`);
  }
  async getAllOffices() {
    const res = await this.getResource(`/org/offices`);
    return res;
  }
  getOffices(id) {
    return this.getResource(`/org/${id}/offices/${id}`);
  }
  async getAllWorkers() {
    const res = await this.getResource(`/org/offices/workers`);
    return res;
  }
  getWorkers(id) {
    return this.getResource(`/org/${id}/offices/${id}/workers/${id}`);
  }
}
